const path = require('path');
const nodeExternals = require('webpack-node-externals');
const WebpackShellPlugin = require('webpack-shell-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

const devMode = process.env.NODE_ENV !== 'production';

const typescriptModule = {
    test: /\.ts$/,
    use: [
        'ts-loader',
    ]
};

const miniCssExtractModule = {
    test: /\.(sa|sc|c)ss$/,
    use: [
        {
            loader: MiniCssExtractPlugin.loader,
            options: {
                // you can specify a publicPath here
                // by default it uses publicPath in webpackOptions.output
                publicPath: '../',
                hmr: devMode,
                filename: devMode ? '[name].css' : '[name].[hash].css',
                chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
                moduleFilename: ({name}) => `${name.replace('/js/', '/css/')}.css`,
            },
        },
        'css-loader',
        'resolve-url-loader',
        {
            loader: 'sass-loader',
            options: {
                sourceMap: true
            }
        }
    ],
};

module.exports = [
    // Node.js
    {
        entry: {
            'server': './server.ts',
        },
        mode: process.env.NODE_ENV,
        target: 'node',
        output: {
            path: path.resolve(__dirname, 'build')
        },
        resolve: {
            extensions: ['.ts', '.js'],
        },
        devtool: 'source-map',
        module: {
            rules: [
                typescriptModule
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new WebpackShellPlugin({
                onBuildEnd: ['yarn run:dev']
            })
        ],
        externals: [nodeExternals()],
        watch: process.env.NODE_ENV === 'development'
    },
    // Web assets
    {
        entry: {
            'js/index': './assets/ts/index.ts',
            'css/styles': './assets/sass/styles.scss'
        },
        mode: process.env.NODE_ENV,
        target: 'node',
        output: {
            path: path.resolve(__dirname, 'public')
        },
        resolve: {
            extensions: ['.ts', '.js', '.sass', '.scss', '.css'],
        },
        module: {
            rules: [
                typescriptModule,
                miniCssExtractModule,
                {
                    test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                        }
                    }]
                },
            ]
        },
        plugins: [
            new CleanWebpackPlugin({
                verbose: true,
                cleanAfterEveryBuildPatterns: ['!img/*']
            }),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // all options are optional
                filename: '[name].css',
                chunkFilename: '[id].css',
                ignoreOrder: false, // Enable to remove warnings about conflicting order
            }),
            new CopyPlugin([
                {from: 'assets/img', to: 'img'}
            ]),
            new ManifestPlugin(),
            {
                apply(compiler)
                {
                    compiler.hooks.shouldEmit.tap('Remove styles from output', (compilation) => {
                        delete compilation.assets['css/styles.js'];  // Remove asset. Name of file depends of your entries and
                        return true;
                    });
                }
            }
        ],
        externals: [nodeExternals()],
        watch: process.env.NODE_ENV === 'development'
    }
];