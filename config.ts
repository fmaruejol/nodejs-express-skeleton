import { config as dotenv } from 'dotenv';
import * as convict from 'convict';
import * as path from 'path';

dotenv();
const config = convict({
    env: {
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV'
    },
    secret : {
        format: String,
        default: 'ThisTokenIsNotSecretChangeIt',
        env: 'SECRET'
    },
    server: {
        host: {
            format: 'url',
            default: '127.0.0.1',
            env: 'HOST'
        },
        port: {
            format: 'port',
            default: 3025,
            env: 'PORT',
        }
    },
    publicDir: {
        format: String,
        default: 'public',
        env: 'PUBLIC_DIR',
    },
    users: {
        admin: {
            username: {
                format: String,
                default: 'admin',
                env: 'ADMIN_USERNAME'
            },
            password: {
                format: String,
                default: '', // Should be a valid bcrypt password
                env: 'ADMIN_PASSWORD'
            }
        }
    }
})

const env = config.get<string>('env');
config.loadFile(`./config/${env}.json`);

config.validate({allowed: 'strict'});

const properties = {
    ...config.getProperties(),
    projectDir: path.resolve('.')
};

export default properties;