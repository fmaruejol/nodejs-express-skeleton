declare namespace Express {
    export interface Response {
        redirectToRoute(routeName: string, params?: object): void
    }
}
