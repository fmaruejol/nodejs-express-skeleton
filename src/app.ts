import container from './inversify.config'

import * as express from 'express';
import { Router } from 'express';
import * as session from 'express-session';
import * as SessionFileStore from 'session-file-store';
import * as bodyParser from "body-parser";
import * as twig from 'twig';
import * as uuid from 'uuid';
import * as passport from "passport";

import config from '../config';
import { TYPES } from "./dependency-injections/types";
import { RouteCollection } from "./routes/route-collection";
import AssetsExtension from "./twig/assets-extension";
import { TwigExtension } from "./twig/twig-extension";
import * as LocalStrategy from "passport-local";
import User from "./models/user";
import * as bcrypt from "bcrypt";
import { Middleware } from "./middlewares/middleware";

const app = express();
const fileStore = new (SessionFileStore(session))();

// Set up view engine to pug
// View engine options
app.set("twig options", {
    allow_async: true, // Allow asynchronous compiling
    strict_variables: false
});

// Set up static files (js, css, images...)
app.use(express.static(config.publicDir));

// Set up post body parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Set up session
app.use(session({
    genid: (req) => {
        return uuid() // use UUIDs for session IDs
    },
    store: fileStore,
    secret: config.secret,
    resave: false,
    saveUninitialized: true
}))

// Set up passport
passport.use(new LocalStrategy.Strategy(
    {usernameField: 'username'},
    (username, password, done) => {
        const user = new User(config.users.admin.username, config.users.admin.password);

        if (username !== user.username) {
            return done(null, false, {message: 'Invalid credentials'});
        }

        if (!bcrypt.compareSync(password, user.password)) {
            return done(null, false, {message: 'Invalid credentials'});
        }

        return done(null, user);
    }
));

// Set up passport user serialization
passport.serializeUser((user: User, done) => {
    done(null, user.username);
});

// Set up passport user deserialization
passport.deserializeUser((username, done) => {
    const user = username === config.users.admin.username ? new User(config.users.admin.username, config.users.admin.password) : null;
    if (user) {
        return done(null, user);
    }

    return done('User not found', false);
});

app.use(passport.initialize());
app.use(passport.session());

// Register own middlewares
for (const middleware of container.getAll<Middleware>(TYPES.Middleware)) {
    app.use(middleware.invoke());
}

// Add routes
const router = Router();
for (const route of container.get<RouteCollection>(TYPES.RouteCollection).routes) {
    router[route.method.toLowerCase()](route.uri, route.handler);
}
app.use(router);

export default app;