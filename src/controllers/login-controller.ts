import { Controller } from "./controller";
import { injectable } from "inversify";
import { route } from "../annotations/route";
import { HttpMethod } from "../routes/route";
import User from "../models/user";
import config from '../../config';

import { NextFunction, Request, Response } from "express";
import * as passport from "passport";
import * as LocalStrategy from 'passport-local';
import * as bcrypt from 'bcrypt';
import { IVerifyOptions } from "passport-local";

@injectable()
export default class LoginController implements Controller {

    @route({uri: '/login', name: 'login', method: HttpMethod.GET, secured: false})
    public login(req: Request, res: Response): void {
        if (req.isAuthenticated()) {
            return res.redirectToRoute('home');
        }

        res.render('pages/login.html.twig', {title: 'Login'});
    }

    @route({uri: '/login', name: 'login_check', method: HttpMethod.POST, secured: false})
    public loginCheck(req: Request, res: Response, next: NextFunction): void {
        if (req.isAuthenticated()) {
            return res.redirect('/');
        }

        passport.authenticate('local', (err, user, info: IVerifyOptions) => {
            if (info) {
                console.log(user);
                return res.render('pages/login.html.twig', {
                    title: 'Login',
                    error: info.message,
                    lastUsername: req.body.username
                });
            }

            if (err) {
                return next(err);
            }

            if (!user) {
                return res.redirectToRoute('login');
            }

            req.login(user, (err) => {
                if (err) {
                    return next(err);
                }

                return res.redirectToRoute('home');
            })
        })(req, res, next);
    }

    @route({uri: '/logout', name: 'logout'})
    public logout(req: Request, res: Response): void {
        req.logout();
        res.redirectToRoute('home');
    }
}