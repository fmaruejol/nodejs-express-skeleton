import { Request, Response } from "express";

import { Logger } from "../utils/logger";
import { inject, injectable } from "inversify";
import { Controller } from "./controller";
import { TYPES } from "../dependency-injections/types";
import { route } from "../annotations/route";

@injectable()
export class IndexController implements Controller {

    @inject(TYPES.Logger) private logger: Logger

    @route({uri: '/', name: 'home'})
    index(req: Request, res: Response) {
        res.render('layout.html.twig', {'title': 'Homepage'});
    }
}
