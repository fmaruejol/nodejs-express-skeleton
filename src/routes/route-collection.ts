import { injectable, multiInject } from "inversify";
import * as util from 'util';

import { Controller } from "../controllers/controller";
import { ROUTES_METADATA_KEY } from "../annotations/route";
import { Route } from "./route";
import { TYPES } from "../dependency-injections/types";

@injectable()
export class RouteCollection {

    private _routes: Array<Route> = [];

    constructor(@multiInject(TYPES.Controller) controllers: Array<Controller>) {
        for (let controller of controllers) {
            /** @type {Array<RouteOptionExtended>} */
            const routes = Reflect.getMetadata(ROUTES_METADATA_KEY, controller.constructor.prototype);
            for (let index in routes) {
                this.routes.push({...routes[index], handler: controller[routes[index].handlerName].bind(controller)});
            }
        }
    }

    get routes(): Array<Route> {
        return this._routes;
    }

    getRouteByName(name: string): Route {
        for (const route of this.routes) {
            if (route.name === name) {
                return route;
            }
        }

        throw new Error(util.format('Route with name "%s" not found', name))
    }

    getRouteByUri(uri: string): Route {
        for (const route of this.routes) {
            if (route.uri === uri) {
                return route;
            }
        }

        throw new Error(util.format('Route with uri "%s" not found', uri))
    }
}