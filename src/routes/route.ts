import { Request, Response } from "express";

export interface Route {
    /**
     * The relative uri to access the route
     */
    uri: string,
    /**
     * The route name
     */
    name: string,
    /**
     * The route method
     */
    method?: HttpMethod,
    /**
     * Whether the user should be authenticated or not
     */
    secured: boolean,
    /**
     * The handler of the route
     */
    handler: (req: Request, res: Response) => void;
}

export enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    PATCH = 'PATCH'
}