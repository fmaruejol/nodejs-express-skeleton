import { HttpMethod } from "../routes/route";

export const ROUTES_METADATA_KEY = 'mvc:routes';

export interface RouteOptions {
    uri: string,
    name: string,
    method?: HttpMethod,
    secured?: boolean,
}

export interface RouteOptionsExtended {
    handlerName: string;
}

export function route(route: RouteOptions) {
    return function (target: Object, propertyKey: string, propertyDescriptor: PropertyDescriptor): PropertyDescriptor {
        let routes: Array<RouteOptionsExtended> = Reflect.getMetadata(ROUTES_METADATA_KEY, target);
        if (!routes) {
            routes = [];
        }

        if (!route.method) {
            route.method = HttpMethod.GET;
        }

        if (route.secured === undefined || route.secured === null) {
            route.secured = true;
        }

        routes.push({...route, handlerName: propertyKey});

        Reflect.defineMetadata(ROUTES_METADATA_KEY, routes, target);

        return propertyDescriptor;
    }
}