import { Container } from "inversify";

import { DefaultLogger, Logger } from "./utils/logger";
import { IndexController } from "./controllers/index-controller";
import { Controller } from "./controllers/controller";
import { TYPES } from "./dependency-injections/types";
import { RouteCollection } from "./routes/route-collection";
import { TwigExtension } from "./twig/twig-extension";
import AssetsExtension from "./twig/assets-extension";
import LoginController from "./controllers/login-controller";
import RouteExtension from "./twig/route-extension";
import { Middleware } from "./middlewares/middleware";
import GenerateRouteMiddleware from "./middlewares/generate-route-middleware";
import SecuredMiddleware from "./middlewares/secured-middleware";
import TwigGlobalMiddleware from "./middlewares/twig-global-middleware";

const container = new Container({
    defaultScope: "Singleton"
});

// Register logger
container.bind<Logger>(TYPES.Logger).to(DefaultLogger);

// Reigster controllers
container.bind<Controller>(TYPES.Controller).to(IndexController);
container.bind<Controller>(TYPES.Controller).to(LoginController);

// Register routes
container.bind<RouteCollection>(TYPES.RouteCollection).to(RouteCollection);

// Register middleware
container.bind<Middleware>(TYPES.Middleware).to(GenerateRouteMiddleware);
container.bind<Middleware>(TYPES.Middleware).to(TwigGlobalMiddleware);
container.bind<Middleware>(TYPES.Middleware).to(SecuredMiddleware);

// Register twig extensions
container.bind<TwigExtension>(TYPES.TwigExtension).to(AssetsExtension);
container.bind<TwigExtension>(TYPES.TwigExtension).to(RouteExtension);

// Invok twig extensions to be registered by twig
container.getAll<TwigExtension>(TYPES.TwigExtension);

export default container;