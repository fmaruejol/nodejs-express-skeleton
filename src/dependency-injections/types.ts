export const TYPES = {
    Controller: Symbol('Controller'),
    Logger: Symbol('Logger'),
    Middleware: Symbol('Middleware'),
    RouteCollection: Symbol('RouteCollection'),
    TwigExtension: Symbol('TwigExtension')
};
