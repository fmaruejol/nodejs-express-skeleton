import { TwigExtension, TwigFunction } from "./twig-extension";
import { injectable } from "inversify";
import * as fs from 'fs';
import * as util from 'util';

import config from '../../config';

@injectable()
export default class AssetsExtension extends TwigExtension {
    getFunctions(): Array<TwigFunction> {
        return [
            new TwigFunction('asset', this.getAsset.bind(this)),
        ];
    }

    async getAsset(path: string) {
        const readFile = util.promisify(fs.readFile);
        const manifest = JSON.parse((await readFile(`${config.projectDir}/${config.publicDir}/manifest.json`)).toString());

        if (manifest.hasOwnProperty(path)) {
            path = manifest[path];
        }

        return path;
    }
}