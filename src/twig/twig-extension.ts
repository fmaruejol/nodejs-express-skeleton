import * as Twig from 'twig';
import { injectable } from "inversify";

@injectable()
export abstract class TwigExtension {
    constructor() {
        for (const twigFunction of this.getFunctions()) {
            Twig.extendFunction(twigFunction.name, twigFunction.handler.bind(twigFunction));
        }

        for (const filter of this.getFilters()) {
            Twig.extendFilter(filter.name, filter.handler.bind(filter));
        }
    }

    getFunctions(): Array<TwigFunction> {
        return [];
    }

    getFilters(): Array<TwigFilter> {
        return [];
    }
}

export class TwigFunction {
    private _name: string;
    private _handler: (...args: any[]) => any;

    constructor(name: string, handler: (...args: any[]) => any) {
        this._name = name;
        this._handler = handler;
    }

    get name(): string {
        return this._name;
    }

    get handler(): (...args: any[]) => any {
        return this._handler;
    }
}

export class TwigFilter {
    private _name: string;
    private _handler: (...args: any[]) => any;

    constructor(name: string, handler: (...args: any[]) => any) {
        this._name = name;
        this._handler = handler;
    }

    get name(): string {
        return this._name;
    }

    get handler(): (...args: any[]) => any {
        return this._handler;
    }
}
