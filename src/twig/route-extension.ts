import { TwigExtension, TwigFunction } from "./twig-extension";
import { inject, injectable } from "inversify";
import * as fs from 'fs';
import * as util from 'util';

import config from '../../config';
import { TYPES } from "../dependency-injections/types";
import { RouteCollection } from "../routes/route-collection";

@injectable()
export default class RouteExtension extends TwigExtension {

    @inject(TYPES.RouteCollection) routeCollection: RouteCollection;

    getFunctions(): Array<TwigFunction> {
        return [
            new TwigFunction('path', this.resolveRoute.bind(this)),
        ];
    }

    async resolveRoute(routeName: string) {
        return this.routeCollection.getRouteByName(routeName).uri;
    }
}