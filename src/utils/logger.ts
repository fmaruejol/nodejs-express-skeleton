import { createLogger, format, Logger as WinstonLogger, transports } from 'winston';
import { injectable } from "inversify";

export interface Logger {
    log(level: LogLevel, message: string): void
    emergency(message: string): void
    alert(message: string): void
    critical(message: string): void
    error(message: string): void
    warning(message: string): void
    notice(message: string): void
    info(message: string): void
    debug(message: string): void
}

export enum LogLevel {
    EMERGENCY = 'emergency',
    ALERT = 'alert',
    CRITICAL = 'critical',
    ERROR = 'error',
    WARNING = 'warning',
    NOTICE = 'notice',
    INFO = 'info',
    DEBUG = 'debug'
}

@injectable()
export class DefaultLogger implements Logger {

    private innerLogger: WinstonLogger;

    public constructor() {
        const winstonLogger: WinstonLogger = createLogger({
            format: format.json(),
            exitOnError: false,
            levels: {
                emergency: 0,
                alert: 1,
                critical: 2,
                error: 3,
                warning: 4,
                notice: 5,
                info: 6,
                debug: 7
            },
            transports: [
                //
                // - Write to all logs with level `info` and below to `combined.log`
                // - Write all logs error (and below) to `error.log`.
                //
                new transports.File({filename: 'var/log/' + process.env.NODE_ENV + '/error.log', level: 'error'}),
                new transports.File({filename: 'var/log/' + process.env.NODE_ENV + '/combined.log'})
            ]
        });

        if (process.env.NODE_ENV !== 'production') {
            winstonLogger.add(new transports.Console({
                level: 'debug',
                format: format.simple()
            }));
        }

        this.innerLogger = winstonLogger;
    }

    log(level: LogLevel, message: string): void {
        this.innerLogger.log({
            level: level,
            message: message
        });
    }

    emergency(message: string): void {
        this.log(LogLevel.EMERGENCY, message);
    }

    alert(message: string): void {
        this.log(LogLevel.ALERT, message);
    }

    critical(message: string): void {
        this.log(LogLevel.CRITICAL, message);
    }

    error(message: string): void {
        this.log(LogLevel.ERROR, message);
    }

    warning(message: string): void {
        this.log(LogLevel.WARNING, message);
    }

    notice(message: string): void {
        this.log(LogLevel.NOTICE, message);
    }

    info(message: string): void {
        this.log(LogLevel.INFO, message);
    }

    debug(message: string): void {
        this.log(LogLevel.DEBUG, message);
    }
}
