import { Middleware } from "./middleware";
import { NextFunction, Request, Response } from "express";
import { inject, injectable } from "inversify";
import { TYPES } from "../dependency-injections/types";
import { RouteCollection } from "../routes/route-collection";

@injectable()
export default class SecuredMiddleware implements Middleware {

    @inject(TYPES.RouteCollection) routeCollection: RouteCollection;

    invoke(): (req: Request, res: Response, next: NextFunction) => void {
        return (req: Request, res: Response, next: NextFunction) => {
            try {
                const currentRoute = this.routeCollection.getRouteByUri(req.url);
                if (currentRoute.secured && !req.isAuthenticated()) {
                    res.redirect(this.routeCollection.getRouteByName('login').uri);
                } else {
                    next();
                }
            } catch (e) {
                next(); // Route not found (assets)
            }

        };
    }
}