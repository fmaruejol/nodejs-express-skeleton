import { inject, injectable } from "inversify";
import { TYPES } from "../dependency-injections/types";
import { RouteCollection } from "../routes/route-collection";
import { NextFunction, Request, Response } from "express";
import { Middleware } from "./middleware";

@injectable()
export default class TwigGlobalMiddleware implements Middleware {
    @inject(TYPES.RouteCollection) routeCollection: RouteCollection;

    invoke(): (req: Request, res: Response, next: NextFunction) => void {
        return (req: Request, res: Response, next: NextFunction) => {
            res.locals = {
                ...res.locals,
                session: req.session,
                user: req.user
            };

            next();
        }
    }
}