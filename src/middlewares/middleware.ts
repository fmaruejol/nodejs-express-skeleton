import { NextFunction, Request, Response } from "express";

export interface Middleware {
    invoke(): (req: Request, res: Response, next: NextFunction) => void;
}