import { inject, injectable } from "inversify";
import { TYPES } from "../dependency-injections/types";
import { RouteCollection } from "../routes/route-collection";
import { NextFunction, Request, Response } from "express";
import { Middleware } from "./middleware";
import { HttpMethod } from "../routes/route";

@injectable()
export default class GenerateRouteMiddleware implements Middleware {
    @inject(TYPES.RouteCollection) routeCollection: RouteCollection;

    invoke(): (req: Request, res: Response, next: NextFunction) => void {
        return (req: Request, res: Response, next: NextFunction) => {
            res.constructor.prototype.redirectToRoute = (name: string, params: object) => {
                const route = this.routeCollection.getRouteByName(name);
                let uri = route.uri;

                if (params && route.method === HttpMethod.GET) {
                    uri += '?';
                    for (let key of Object.keys(params)) {
                        uri += uri.substr(uri.length - 1) === '?' ? `${key}=${params[key]}` : `&${key}=${params[key]}`
                    }
                }

                res.redirect(uri);
            }

            next();
        }
    }
}