// Important should be imported first and only time in the whole project
import "reflect-metadata";
import config from "./config";
import * as http from 'http';

import app from './src/app';
import { TYPES } from "./src/dependency-injections/types";
import container from './src/inversify.config'

import {Logger} from "./src/utils/logger";

const logger = container.get<Logger>(TYPES.Logger);

const host = config.server.host;
const port = config.server.port;

app.listen(port, host,() => {
    logger.info('Server is listening on http://' + host + ':' + port);
});